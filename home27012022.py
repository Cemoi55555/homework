# Задача1:
# Есть строка произвольного содержания. Написать код, который найдет в строке самое короткое слово,
# в котором присутствуют подряд две гласные буквы.

enter_string = input('Введите строку: ') # Entering string
signs = ',.?!:;'# enter a string in which we indicate the characters that we want to exclude
for sign in signs: # Using a loop, replace the characters with an empty string in "enter_string"
    enter_string.replace(signs, '')
string = enter_string.split() # break the string into parts
vowel_letter = ['e', 'y', 'u', 'i', 'o,', 'a', 'E', 'Y', 'U', 'I', 'O', 'A', 'у', 'е', 'ї','і', 'а', 'о', 'є', 'я', 'и',
                'ю', 'У', 'Е', 'Ї', 'І', 'А', 'О', 'Є', 'Я', 'И', 'Ю', 'у', 'е', 'ы', 'а', 'о', 'э', 'ё', 'я', 'и',
                 'ю', 'Ё', 'У', 'Е', 'Ы', 'А', 'О', 'Э', 'Я', 'И', 'Ю']# enter a list with vowel letter
selected_word = [] # create empty list
counter = 0 # create counter with position zero
for word in string: # Using a loop,sort through the words in "string"
    for let in range(0,len(word)):# Using a loop, iterate letters in a word by index
        if word[let] in vowel_letter: # if there is a value of a letter under such an index, then add 1 to the counter
           counter += 1
           if counter == 2: #if the counter value is 2
               selected_word.append(word)# then add the word to "selected_word"
               continue
        if word[let] not in vowel_letter: #if there is not a value of a letter - counter = 0
            counter = 0
            # print('No matches')

min_word = min(selected_word, key=len) # find the minimum length of a word in "selected_word" using key len
print(f'Short word with two vowel letter is: {min_word}') #Displaying a message with answer

# Задача2:
# Есть два числа - минимальная цена и максимальная цена.
# Дан словарь продавцов и цен на какой то товар у разных продавцов:
# { "citrus": 47.999, "istudio" 42.999, "moyo": 49.999, "royal-service": 37.245, "buy.ua": 38.324, "g-store": 37.166,
# "ipartner": 38.988, "sota": 37.720, "rozetka": 38.003}.
# Написать код, который найдет и выведет на экран список продавцов, чьи цены попадают в диапазон между нижней и верхней ценой.
# Например:
# lower_limit = 35.9
# upper_limit = 37.3
# > match: "g-store", "royal-service"

home_dict = {
    "citrus": 47.999,
    "istudio": 42.999,
    "moyo": 49.999,
    "royal-service": 37.245,
    "buy.ua": 38.324,
    "g-store": 37.166,
    "ipartner": 38.988,
    "sota": 37.720,
    "rozetka": 38.003
}
while True: # Using a loop, "while" we check the correctness of user input using the construction "try/except"
    try: #  exclude user input of non-numbers
        lower_limit = float(input('Enter min price: '))
        upper_limit = float(input('Enter max price: '))
    except: # If an exception has been thrown and the number is not converted from the string
        print('Введите число')
        continue # Repeat input if not a number
    break # Exit the loop if the numbers are entered correctly
if lower_limit >= upper_limit: # check the condition if the user entered incorrect data
    print('Enter exact numbers!')
answers = [] # create empty list
for key, values in home_dict.items(): # Using a loop, sorting through the values "key" and "values" in home_dict
    if values > lower_limit and values < upper_limit:# check the condition values > lower_limit and values < upper_limit
        answers.append(key) # then add the "key" to "answer"
print(f'> match:{answers}')  # Displaying a message with answer

