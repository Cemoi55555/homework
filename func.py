
def ask_user():#create function which checks the answer
    """
    This function asks the user possible answer 'yes' or 'no'
    answers_yes: possible user answer 'yes'
    answers_no: possible user answer 'no'
    :return: boolean value True or False
    """
    answers_yes = ['yes', 'ye', 'y']
    answers_no = ['no', 'n',]
    response_low = ''
    while response_low not in answers_yes or answers_no: #loop while check the condition if not in answers_yes or answers_no
        response = input("Y/N: ") # ask Y/N
        response_low = response.lower() # the entered answer is translated into small letters
        if response_low in answers_yes: # if the user responded 'yes', 'ye', 'y'
            return True # return True
        if response_low in answers_no: # if the user responded 'no', 'n'
            return False #return False

if __name__ == '__main__': # the function will be executed only if it is called from the main file
    ask_user()

def user_corect_enter():
    """
    This function asks the user to enter a number from 1 to 100 and returns it.
    If the user entered not a number or not from the range from 1 to 100 indicates invalid input
    return: integer number from 1 to 100
    """
    while True:  #  Using the while loop, we check the correctness of the user's input using the try / except construct
        try:  # I put age and a while loop in the try block to prevent the user from entering non-numbers
            user_enter = int(input('Угадай число от 1 до 100: '))  #asking the player to enter a number from 1 - 100
        except:  # If an exception has been thrown and the number is not converted from the string
            print('Введите число используя цыфры')
            continue  # Repeat input if not a number
        break  # Exit the loop if the numbers are entered correctly
    while user_enter <= 0 or user_enter >= 101:  # Using the while loop checks if the user entered a value less than
        print('Вы задали число вне диапазона 1-100!')  # We display a message if the user entered zero or negative values
        user_enter = int(input('Угадай заданое число от 1 до 100: '))
    return user_enter

if __name__ == '__main__': # the function will be executed only if it is called from the main file
    user_corect_enter()

