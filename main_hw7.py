# Задача
# Пишем игру. Программа выбирает из диапазона чисел (пусть для начала будет 1-100) случайное число и предлагает пользователю
# его угадать. Пользователь вводит число. Если пользователь не угадал - предлагает пользователю
# угадать еще раз, пока он не угадает. Если угадал - спрашивает хочет ли он повторить игру (Y/N).
# Если Y - повторить. N - Прекратить. Опционально - добавьте в задание вывод сообщения-подсказки.
# Если пользователь ввел число, и не угадал - сообщать:
# "Холодно" если разница между загаданным и введенным числами больше 10, "Тепло" - если от 5 до 10 и
# "Горячо" если от 4 до 1.
#
#
# П.С. У вас уже есть знание функций так что все выполнение можно разбить на функции.
# Постарайтесь чтобы функции выполняли только одну задачу. Не используйте рекурсию.
# Случайными значениями занимается модуль random (https://docs.python.org/3/library/random.html)

import random
import func
from func import ask_user
from func import user_corect_enter

def find_number():
    """
    The program selects a random number from a range of numbers from 1 to 100  and prompts the user
    guess him.
    """
    while True: # using a loop the program will be repeated until the user answers no
        number_range = random.randint(1, 100) # the function  random.randint generates a random integer from 1 to 100
        user_enter = None # Initially set user input None
        while user_enter != number_range: # using a loop while compare generated number with input
            user_enter = user_corect_enter() # call user_corect_enter for users variant number
            number_difference = abs(number_range - user_enter)  # make all results number difference positive using abs
            if number_difference == 5 or number_difference == 6 or number_difference == 7 or number_difference == 8 or number_difference == 9:
                print("Тепло!")
            elif number_difference >= 10:
                print("Холодно!")
            elif number_difference == 1 or number_difference == 2 or number_difference == 3 or number_difference == 4:
                print("Горячо!")
            elif number_difference == 0:
                print(f'Вы угадали, это число = {number_range}')
                break
        print(f'Хотите ли Вы повторить  игру (Y/N)?')  # bring it to the screen
        answer = ask_user()  # call function ask_user()
        if answer == False:  # when user says N
            print('Прекратить')  # bring it to the screen 'Завершено'
            break  # and stop
print(find_number())