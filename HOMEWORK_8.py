# Задача Доработайте игру с занятия следующим образом: добавьте новые игровые фигуры (камень ножницы, бумага ->
# камень, ножницы, бумага, ящерица, Спок) https://www.youtube.com/watch?v=A2BYHWFpgVQ .
# Также игра должна записывать данные статистики в текстовый файл (пронумерованный список: дата и время, кто выиграл, что выбросили).
# Запись в файл https://docs.python.org/3/library/functions.html#open или https://pythonworld.ru/tipy-dannyx-v-python/fajly-rabota-s-fajlami.html

import random
import datetime
from random import choice as random_choice, shuffle
from datetime import datetime

def user_choice(*variants):
    """
    Тhis function is needed to receive a selection from the user
    :param variants:
    :return:  choice of user
    """
    msg = f'Choose one of: {",".join(variants)}'# proposed options
    while True: # using a loop the program will be repeated until the user entered variants
        print(msg)
        user_input = input('Your choice is: ')
        if user_input not in variants:# if you enter something else
            print('Wrong!')
        else:
            return user_input

def computer_choice(*variants):
    """
    This function is needed to receive a selection from the computer
    :param variants:
    :return: choice of computer(random choice in variants
    """
    variants = list(variants)
    shuffle(variants)

    result = random_choice(variants)

    return result

def is_user_win(rules_win, rules_winlss, user_figure, computer_figure):
    """

    :param rules_win:
    :param user_figure:
    :param computer_figure:
    :return:
        (bool|None)
    """
    if user_figure == computer_figure:
        return
    if rules_win[user_figure] == computer_figure:
        return True
    if rules_winlss[user_figure] == computer_figure:
        return True
    return False

def make_message(result, user_figure, computer_figure):
    """

    :param result:
    :param user_figure:
    :param computer_figure:
    :return: message based on answer option from dct
    """
    dct = {
        True: 'User Win!',
        False: 'Computer Win!',
        None: 'Draw!'
    }
    msg = f' Result: {dct[result]} user choice - {user_figure}, computer choice - {computer_figure}'
    return msg


def date_time():
    """

    :return: current date and time
    """
    time = datetime.today().strftime("%H:%M:%S")
    data = datetime.today().strftime("%d.%m.%Y")
    data_time = f'{time}  {data}'
    return data_time



def RSP_game():
    """
    main function of the game
    :return: message with wins and choice also write in data.txt information about current date and time with wins and choice
    """
    figures = ('Rock', 'Scissors', 'Paper', 'Lizard', 'Spock') # choices
    rules_win = {                               #rules of game
        'Rock': 'Scissors',
        'Scissors': 'Paper',
        'Paper': 'Rock',
        'Lizard': 'Spock',
        'Spock': 'Scissors',

    }
    rules_winlss = {                            #rules of game
        'Rock': 'Lizard',
        'Scissors': 'Lizard',
        'Paper': 'Spock',
        'Lizard': 'Paper',
        'Spock': 'Rock',

    }

    user_figure = user_choice(*figures) # what the user has chosen
    computer_figure = computer_choice(*figures)# what the computer has chosen
    result = is_user_win(rules_win, rules_winlss, user_figure, computer_figure) # calling is_user_win function  determine the winner

    message = make_message(result, user_figure, computer_figure) # display a message about the winner or a draw calling make_message function
    txt = open('data.txt', 'a') # open a file with the ability to append
    txt.write(str(date_time()) + message +'\n') #write to a file with time, date and  message about the winner or a draw and choices
    txt.close()#
    return message
result = RSP_game()
print(result)

