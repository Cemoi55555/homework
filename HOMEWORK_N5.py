# Задача 1
# Написать функцию, принимающую два аргумента. Функция должна :
# - если оба аргумента относятся к числовым типам - вернуть их произведение,
# - если к строкам - соединить в одну строку и вернуть,
# - если первый строка, а второй нет - вернуть словарь (dict), в котором ключ - первый аргумент, значение - второй
# в любом другом случае вернуть кортеж (tuple) из аргументов

#
def home_work5_1(first, second):
   if type(first) == int or float and type(second) == int or float: #  check the condition if both values of the number
      return first * second #return their product
   if type(first) == str and type(second) == str: # if both arguments are string
      return first + second # return their join in one line
   if type(first) == str and type(second) == int or float: #if the first is a string and the second is not
      dictionary = {}# create a dictionary
      dictionary.setdefault(first, second)
      return dictionary # return a dictionary
   else:
      tuple = (first, second) # in any other case
      return tuple # # return a tuple

print(home_work5_1('sdf', 2)) # call the function


# Задача 2
# Пользователь вводит строку произвольной длины. Написать функцию, которая принимает строку и должна вернуть словарь следующего содержания:
# ключ - количество букв в слове, значение - list слов с таким количеством букв.
# отдельным ключем, например "0", записать количество пробелов.
# отдельным ключем, например "punctuation", записать все уникальные знаки препинания, которые есть в тексте.
# Например:
#
# {
#
# "0": количество пробелов в строке
#
# "1": list слов из одной буквы
#
# "2": list слов из двух букв
#
# "3": list слов из трех букв
#
# и т.д ...
#
# "punctuation" : tuple уникальных знаков препинания
#
# }
#
#
import re # import regular expressions

user_input = input(f'Enter string: ') # User entering string

def find_num_space(): # create function which  will find the number of spaces in a string

    find_space = re.findall(r' ', user_input) # using regular expressions find all spaces in a string
    num_zero = len(find_space) #find out the number of spaces
    return num_zero # return out the number of spaces
print(find_num_space()) # call the 'function find_num_space()'

def find_num_punctuation():# create function which  will find the number of punctuation in a string

    find_punctuation = re.findall(r'[!?:;.,]', user_input)# using regular expressions find indicated punctuation in a string
    punctuation = len(find_punctuation) #find out the number of punctuation
    return punctuation # return out the number of punctuation
print(find_num_punctuation()) # call the function 'find_num_punctuation()'

def home_work5_2():# create function which fulfills homework requirement
    diction = {}  # create an empty dictionary
    num_zero = find_num_space() # call the 'function find_num_space()'
    diction[0] = num_zero # add value to dictionary number of zero
    string = user_input.split() # split string with spaces
    for word in string: # loop through the string
        key = len(word) # enter a variable that is equal to the length of the word
        diction.setdefault(key, []).append(word)#the dictionary is filled with values in which the variable 'key' is the default value of the key
    sorted_tuple = sorted(diction.items(), key=lambda x: x[0])  # sort keys in ascending order creating a tuple
    diction = dict(sorted_tuple) #from the sorted tuple we display a dictionary
    num_punctuation = find_num_punctuation()
    diction['punctuation'] = num_punctuation # add value to dictionary number of punctuation
    return diction # return diction
print(home_work5_2()) # call the function