
def ask_user():#create function which checks the answer
    answers_yes = ['yes', 'ye', 'y']
    answers_no = ['no', 'n',]
    response_low = ''
    while response_low not in answers_yes or answers_no: #loop while check the condition if not in answers_yes or answers_no
        response = input("Y/N: ") # ask Y/N
        response_low = response.lower() # the entered answer is translated into small letters
        if response_low in answers_yes: # if the user responded 'yes', 'ye', 'y'
            return True # return True
        if response_low in answers_no: # if the user responded 'no', 'n'
            return False #return False

if __name__ == '__main__': # the function will be executed only if it is called from the main file
    ask_user()

