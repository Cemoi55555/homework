import lib
from lib import ask_user

# В отдельном файле (пусть будет lib.py) написать функцию, которая требует от пользователя ответить да или нет (Y/N)
# и возвращает True/False в зависимости от того, что он ввел. В основном файле (пусть будет main_file.py)
# попросить пользователя ввести с клавиатуры строку и вывести ее на экран.
# Используя импортированную из lib.py функцию спросить у пользователя, хочет ли он повторить операцию (Y/N).
# Повторять пока пользователь отвечает Y и прекратить когда пользователь скажет N.
#
while True: # using loop while check the condition if repeat until user says Y and stop when user says N
    enter_string = input('Введите строку: ')#ask the user to enter a string from the keyboard
    print(f'Вы ввели {enter_string}, хотите ли Вы повторить операцию?') # bring it to the screen
    answer = ask_user() # call function ask_user()
    if answer == False: # when user says N
        print('Завершено')# bring it to the screen 'Завершено'
        break # and stop

# Модифицируем ДЗ 2. Напишите с помощью функций!. Помните о Single Responsibility! Попросить ввести свой возраст (можно использовать константу или input()).
# Пользователь ввел значение возраста [year number] а на место [year string] нужно поставить правильный падеж существительного "год",
# который зависит от значения [year number].
# если пользователь ввел непонятные данные (ничего не ввел, ввел не число, неактуальный возраст и тд.) - вывести “не понимаю”
# если пользователю меньше 7 - вывести “Тебе [year number] [year string], где твои мама и папа?”
# если пользователю меньше 18 - вывести “Тебе [year number] [year string], а мы не продаем сигареты несовершеннолетним”
# если пользователю больше 65 - вывести “Вам уже [year number] [year string], вы в зоне риска”!
# в любом другом случае - вывести “Оденьте маску, вам же [year number] [year string]!”
# Например:
#
# Тебе 1 год, где твои мама и папа?
#
# Оденьте маску, вам же 23 года!
#
# Вам уже 68 лет, вы в зоне риска!

# Ну и как обычно чистота кода, читаемость, понятность - наше все !
#


def int_age_more_zero_func(): # Function 'int_age_more_zero_func' checks if the entered number is correct
    age = ''
    while True:  # Using the while loop, we check the correctness of the user's input using the try / except constructС
        try:  # I put age and a while loop in the try block to prevent the user from entering non-numbers
            age = int(input('Пожалуйста введите свой возраст: '))
        except:  # If an exception has been thrown and the number is not converted from the string
            print('Не понимаю')
            continue  # Repeat input if not a number
        break  # Exit the loop if the numbers are entered correctly
    while age <= 0:  # Using the while loop checks if the user entered a value less than
        print('Не понимаю')  # We display a message if the user entered zero or negative values
        age = int(input('Пожалуйста введите еще раз свой возраст: '))
    return age

def year_string_func(): # Function "year_string_func" substitutes values 'год' 'года' 'лет'
    age = int_age_more_zero_func() # call Function 'int_age_more_zero_func' to get the value "age"
    if age == 1 or str(age)[-1] == '1':
        year = f'год'
        return year
    if age >= 2 and age <= 4:
        year = f'года'
        return year
    if str(age)[-1] == '2' or str(age)[-1] == '3' or str(age)[-1] == '4':
        year = f'года'
        return year
    if age % 10 == 0:
        year = f'лет'
        return year
    if age >= 5 and age < 21:
        year = f'лет'
        return year
    if str(age)[-1] == '5' or str(age)[-1] == '6' or str(age)[-1] == '7' or str(age)[-1] == '8' or str(age)[-1] == '9':
        year = f'лет'
        return year

def age_control(): # Function "age_control" fulfills the condition of the task

    age = int_age_more_zero_func() # call Function 'int_age_more_zero_func' to get the value "age"
    year_string = year_string_func() # call Function "year_string_func()" substitutes values 'год' 'года' 'лет'
    if age >= 1 and age < 7:  # Check the condition if the user is less than 7
        message = f'Тебе {age} {year_string}, где твои мама и папа?'  #We display a message if the user is less than 7
        return message
    elif age >= 7 and age < 18:  # We check the condition if the user is less than 18
        message = f'Тебе {age} {year_string}, а мы не продаем сигареты несовершеннолетним'#We display a message if the user is less than 18
        return message
    elif age > 65:  # Check the condition if the user is over 65
        message = f'Вам уже {age} {year_string}, вы в зоне риска!'  # We show a message if the user is over 65
        return message
    else:  # in any other case
        message = f'Оденьте маску, вам же {age} {year_string}!' # We show a message
        return message

print(age_control()) # output the results
